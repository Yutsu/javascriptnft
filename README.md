Equipe :
Tan Edwina / edwina.tan@mail-ecv.fr
Raheriarisoa Eliott / eliott.raheriarisoa@mail-ecv.fr
Ratsimandresy Frederico / ratsim.rico@gmail.com

Section : 
- Si vous n'avez pas VS Code, téléchargez et installez-le
- Installer l'extension Live Server et activez-la
- Ouvrir le dossier du projet
- Sur la barre d'état en bas à droite, cliquez sur "Go Live", ou clic droit sur le fichier "index.html" à la racine du projet et sélectionnez "Open with Live Server"

Fonctionnalités :

Lister les produits : Edwina
Voir les détails d’un produit : Eliott
Filtrer par créateur : Rico
Filtrer par nombre de vente : Edwina
Ajouter / Supprimer des favoris / persister dans le LS : Rico
Afficher les collections : Rico
Pagination : Rico
Coder une fonctionnalité : All
Utilisation d’une lib : All
Améliorer la méthode vue en cours pour la création de component : All
Intégration d’un lazy loading sans lib : Rico
Persister les fav dans le LS : Rico
Système de routing : Eliott
Chercher des produits dans la barre de recherche : Eliott
