export const checkElement = (element) => {
  if (!element) {
    return
  }
  return element
}

export const addStyle = (selectedElement, property, value) => {
  selectedElement.style[`${property}`] = `${value}`
}

export const createElement = (
  element,
  parent,
  property = null,
  value = null
) => {
  const body = document.getElementsByTagName("body")[0]
  const newElement = document.createElement(element)
  property && value ? addStyle(newElement, property, value) : newElement
  if (parent) {
    return parent.appendChild(newElement)
  }
  return body.append(newElement)
}

export const getId = (selectElement) => {
  const element = document.getElementById(selectElement)
  return checkElement(element)
}

export const getAnyElement = (selectElement) => {
  const element = document.querySelector(selectElement)
  return checkElement(element)
}

export const renderArticleCard = (data) => {
  const { image_url, name, sales, id } = data
  return `
    <article id=${id} data-aos-once="true" data-aos="zoom-in" class="article">
      <img class="card__img" alt="card image" src=${image_url} />
      <p class="card__name">${name}</p>
      <svg
      class="favorite"
      width="24px"
      height="24px"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 122.88 107.39"
    >
    <path
    fill="currentColor"
    fill-rule="evenodd"
    d="M60.83,17.18c8-8.35,13.62-15.57,26-17C110-2.46,131.27,21.26,119.57,44.61c-3.33,6.65-10.11,14.56-17.61,22.32-8.23,8.52-17.34,16.87-23.72,23.2l-17.4,17.26L46.46,93.55C29.16,76.89,1,55.92,0,29.94-.63,11.74,13.73.08,30.25.29c14.76.2,21,7.54,30.58,16.89Z"
    />
    </svg>
    <div class="card__container__price">
    <p class="card__sales">&nbsp;${sales.toLocaleString()}</p>
    </div>
    </article>
    `
}

export const renderCircleLoader = () => {
  return `
  <svg
  class="circle_loader"
  xmlns="http://www.w3.org/2000/svg"
  width="100px"
  height="100px"
  x="0px"
  y="0px"
  viewBox="0 0 100 100"
  enable-background="new 0 0 0 0"
  xml:space="preserve"
>
  <path
    fill="#fff"
    d="M73,50c0-12.7-10.3-23-23-23S27,37.3,27,50 M30.9,50c0-10.5,8.5-19.1,19.1-19.1S69.1,39.5,69.1,50"
  >
    <animateTransform
      attributeName="transform"
      attributeType="XML"
      type="rotate"
      dur="1.5s"
      from="0 50 50"
      to="360 50 50"
      repeatCount="indefinite"
    />
  </path>
</svg>
  `
}

export const renderDottedLoader = () => {
  return `
  <svg class="dotted_loader" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
  viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
  <circle fill="#fff" stroke="none" cx="6" cy="50" r="6">
    <animate
      attributeName="opacity"
      dur="1s"
      values="0;1;0"
      repeatCount="indefinite"
      begin="0.1"/>    
  </circle>
  <circle fill="#fff" stroke="none" cx="26" cy="50" r="6">
    <animate
      attributeName="opacity"
      dur="1s"
      values="0;1;0"
      repeatCount="indefinite" 
      begin="0.2"/>       
  </circle>
  <circle fill="#fff" stroke="none" cx="46" cy="50" r="6">
    <animate
      attributeName="opacity"
      dur="1s"
      values="0;1;0"
      repeatCount="indefinite" 
      begin="0.3"/>     
  </circle>
</svg>  
  `
}

export const renderNftFavCard = (data) => {
  const { image_url, name, sales, id } = data
  return `
  <article data-aos-once="true" data-aos="fade-right" data-aos-duration="1000" id='${id}' class="fav_card">
    <svg
      class="favorite is"
      width="24px"
      height="24px"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 122.88 107.39"
    >
      <path
        fill="currentColor"
        fill-rule="evenodd"
        d="M60.83,17.18c8-8.35,13.62-15.57,26-17C110-2.46,131.27,21.26,119.57,44.61c-3.33,6.65-10.11,14.56-17.61,22.32-8.23,8.52-17.34,16.87-23.72,23.2l-17.4,17.26L46.46,93.55C29.16,76.89,1,55.92,0,29.94-.63,11.74,13.73.08,30.25.29c14.76.2,21,7.54,30.58,16.89Z"
      />
    </svg>
    <div class="fav_img">
      <img height="100px" width="100px" src='${image_url}' alt='${name}'/>
    </div>
    <div class="fav_info">
      <p>${name}</p>
      <p>
      sales:&nbsp;${sales}
      </p>
    </div>
  </article>
  `
}

export const renderCollectionArticle = (data) => {
  const { image_url, name, slug } = data
  return `
    <article id=${slug} data-aos="zoom-in" class="collections">
      <img class="collection_image" src=${image_url} alt=${name}/>
      <p class="collection_name">${name}</p>
      <img src="/Assets/iconCollections.svg" class="collection_icon" alt="${slug}"/>
    </article>
  `
}

export const renderCreatorCard = (creator) => {
  const { username, profile_url, address } = creator[0]
  return `
    <img data-aos-delay="200" data-aos-once="true" data-aos="fade-in" src=${profile_url} alt=${username} id="profil_img"/>
    <footer>
      <p data-aos="fade-right" data-aos-delay="300">Creator:&nbsp;<strong class="creator_name">${username}</strong></p>
      <p data-aos="fade-left" data-aos-delay="400" class="creator_address">Address: ${address}</p>
    </footer>
 `
}

const chart = getAnyElement(".container.chart")

export const isGraphHidden = () => {
  chart.classList.add("isHidden")
}

export const isGraphDisplayed = () => {
  chart.classList.remove("isHidden")
}
