import { favoritesPage } from "../Pages/Favorites/index.js"
import { creators } from "../Pages/Filter/Creator/index.js"
import { sales } from "../Pages/Filter/Sales/index.js"
import { renderAllGraph } from "../Pages/Graph/index.js"
import { home } from "../Pages/Home/index.js"
import { isGraphDisplayed, isGraphHidden } from "../Utils/index.js"

const route = (event) => {
  if (!event) {
    event = window.event
    event.preventDefault()
    window.history.pushState({}, "", event.target.href)
    isGraphHidden()
    handleLocation()
    home()
    if (event.target.innerHTML) {
      isGraphHidden()
      home()
    }
  } else {
    window.history.pushState({}, "", `${event}`)
    isGraphHidden()
    handleLocation()
    switch (event) {
      case "/filterCreators":
        creators()
        break
      case "/filterSales":
        sales()
        break
      case "/favorites":
        favoritesPage()
        break
      case "/graph":
        isGraphDisplayed()
        renderAllGraph()
        break
      default:
        break
    }
  }
}

const routes = {
  404: "/pages/404/index.html",
  "/": "/pages/home/index.html",
  "/index.html": "/pages/home/index.html",
  "/favorites": "/pages/favorites/index.html",
  "/graph": "/pages/graph/index.html",
  "/filterCreators": "/pages/filter/creator/index.html",
  "/filterSales": "/pages/filter/sales/index.html",
  "/nft": "/pages/nft/index.html",
}

const handleLocation = async () => {
  const path = window.location.pathname
  const route = routes[path] || routes[404]
  const html = await fetch(route).then((data) => data.text())
  document.getElementById("main-page").innerHTML = html

  path == "/" || path == "/index.html"
    ? (document.title = "NFT Project")
    : (document.title = path.split("/")[1])

  path == "/nft"
    ? (document.title = "NFT's details")
    : (document.title = path.split("/")[1])

  path == "/favorites"
    ? (document.title = "Favorites")
    : (document.title = path.split("/")[1])

  path == "/filterCreators"
    ? (document.title = "Creators")
    : (document.title = path.split("/")[1])

  path == "/graph"
    ? (document.title = "Graphs")
    : (document.title = path.split("/")[1])

  path == "/filterSales"
    ? (document.title = "Sales")
    : (document.title = path.split("/")[1])
}

window.onpopstate = handleLocation
window.route = route

handleLocation()
