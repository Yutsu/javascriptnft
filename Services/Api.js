export const fetchData = async (value) => {
  try {
    let data
    const response = await fetch(
      `https://awesome-nft-app.herokuapp.com/${value}`,
      {
        headers: {
          "Content-type": "application/json",
        },
      }
    )
    if (!response.ok) {
      throw new Error(`HTTP error! status: ${response.status}`)
    } else {
      data = await response.json()
      return data
    }
  } catch (error) {
    console.log(`${error.message}: ${response.status}`)
  }
}

export const getAllNft = async () => {
  const N = [1, 2, 3, 4, 5]
  const all = N.map(async (n, i) => {
    const nft = await fetchData(`?page=${n}`)
    if (~i) {
      return (N[i] = nft.assets)
    }
  })
  const allNFT = await Promise.all([...all].flat(1)).then((res) => res)
  return allNFT.flat(1)
}

export const getNftPerPage = async (n) => {
  return await fetchData(`?page=${n}`)
}
