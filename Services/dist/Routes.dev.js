"use strict";

var _index = require("../Pages/Favorites/index.js");

var _index2 = require("../Pages/Filter/Creator/index.js");

var _index3 = require("../Pages/Filter/Sales/index.js");

var _index4 = require("../Pages/Graph/index.js");

var _index5 = require("../Pages/Home/index.js");

var _index6 = require("../Utils/index.js");

var route = function route(event) {
  if (!event) {
    event = window.event;
    event.preventDefault();
    window.history.pushState({}, "", event.target.href);
    (0, _index6.isGraphHidden)();
    handleLocation();
    (0, _index5.home)();

    if (event.target.innerHTML) {
      (0, _index6.isGraphHidden)();
      (0, _index5.home)();
    }
  } else {
    window.history.pushState({}, "", "".concat(event));
    (0, _index6.isGraphHidden)();
    handleLocation();

    switch (event) {
      case "/filterCreators":
        (0, _index2.creators)();
        break;

      case "/filterSales":
        (0, _index3.sales)();
        break;

      case "/favorites":
        (0, _index.favoritesPage)();
        break;

      case "/graph":
        (0, _index6.isGraphDisplayed)();
        (0, _index4.renderAllGraph)();
        break;

      default:
        break;
    }
  }
};

var routes = {
  404: "/pages/404/index.html",
  "/": "/pages/home/index.html",
  "/index.html": "/pages/home/index.html",
  "/favorites": "/pages/favorites/index.html",
  "/graph": "/pages/graph/index.html",
  "/filterCreators": "/pages/filter/creator/index.html",
  "/filterSales": "/pages/filter/sales/index.html",
  "/nft": "/pages/nft/index.html"
};

var handleLocation = function handleLocation() {
  var path, route, html;
  return regeneratorRuntime.async(function handleLocation$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          path = window.location.pathname;
          route = routes[path] || routes[404];
          _context.next = 4;
          return regeneratorRuntime.awrap(fetch(route).then(function (data) {
            return data.text();
          }));

        case 4:
          html = _context.sent;
          document.getElementById("main-page").innerHTML = html;
          path == "/" || path == "/index.html" ? document.title = "NFT Project" : document.title = path.split("/")[1];
          path == "/nft" ? document.title = "NFT's details" : document.title = path.split("/")[1];
          path == "/favorites" ? document.title = "Favorites" : document.title = path.split("/")[1];
          path == "/filterCreators" ? document.title = "Creators" : document.title = path.split("/")[1];
          path == "/graph" ? document.title = "Graphs" : document.title = path.split("/")[1];
          path == "/filterSales" ? document.title = "Sales" : document.title = path.split("/")[1];

        case 12:
        case "end":
          return _context.stop();
      }
    }
  });
};

window.onpopstate = handleLocation;
window.route = route;
handleLocation();