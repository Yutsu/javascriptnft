import { home } from "./Pages/Home/index.js"
import { favoritesPage } from "./Pages/Favorites/index.js"
import {
  getAnyElement,
  renderArticleCard,
  isGraphHidden,
} from "./Utils/index.js"
import { fetchData, getAllNft } from "./Services/Api.js"

home()
favoritesPage()
AOS.init()

const searchbar = document.getElementById("searchbar")
const mainpage = document.getElementById("main-page")
const searchElements = document.getElementById("searchElements")

const clickToSearch = (e) => {
  e.target.style.position = "absolute"
  e.target.style.left = "0"
  e.target.style.top = "5rem"
  e.target.style.width = "100%"

  if (mainpage.style.display != "none") {
    mainpage.style.display = "none"
    searchElements.style.display = "flex"
  }

  window.addEventListener("click", function removeSearch(event) {
    if (event.target.tagName != "INPUT") {
      e.target.style.removeProperty("position")
      window.removeEventListener("click", removeSearch)
      if (mainpage.style.display == "none") {
        mainpage.removeAttribute("style")
        searchElements.removeAttribute("style")
      }
    }
  })
}

searchbar.onclick = clickToSearch

const main = async () => {
  //fetch data
  let arrAssets = []
  const displaySearchNFT = async () => {
    const arr = await getAllNft()
    arrAssets = arr.map((el) => {
      searchcard.innerHTML += renderArticleCard(el)
      return { name: el.name }
    })
  }

  displaySearchNFT()
  isGraphHidden()

  // search cards
  const searchcard = getAnyElement(".searchcard")

  //search Input
  searchbar.addEventListener("input", (e) => {
    const value = e.target.value.toLowerCase()

    arrAssets.forEach((el, index) => {
      const isVisible = el.name.toLowerCase().includes(value)
      searchcard.children[index].classList.toggle("hide", !isVisible)
    })
  })

  searchbar.addEventListener("keypress", (event) => {
    if (event.key === "Enter") {
      event.preventDefault()
    }
  })
}

main()
