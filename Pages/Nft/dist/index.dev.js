"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.nft = void 0;

var _Api = require("../../Services/Api.js");

var _index = require("../../Utils/index.js");

//Single NFT
var nft = function nft() {
  var paramsQuery, urlParams, id, fetch, displayData, date;
  return regeneratorRuntime.async(function nft$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          paramsQuery = window.location.search;
          urlParams = new URLSearchParams(paramsQuery);
          id = urlParams.get("id");
          _context.next = 5;
          return regeneratorRuntime.awrap((0, _Api.fetchData)("nft/".concat(id)));

        case 5:
          fetch = _context.sent;

          displayData = function displayData(el, type, data) {
            if (type == "text" && (0, _index.getAnyElement)(el)) {
              return (0, _index.getAnyElement)(el).innerHTML = data;
            } else if (type == "image" && (0, _index.getAnyElement)(el)) {
              return (0, _index.getAnyElement)(el).setAttribute("src", data);
            }
          };

          fetch.name ? displayData("#NFT__second__principal__name", "text", fetch.name) : displayData("#NFT__second__principal__name", "text", "No name");
          fetch.description ? displayData("#NFT__description", "text", fetch.description) : displayData("#NFT__description", "text", "No description");
          fetch.image_url ? displayData("#NFT__image", "image", fetch.image_url) : "";
          fetch.sales ? displayData("#NFT__second__secondary__nbSales", "text", fetch.sales) : displayData("#NFT__second__secondary__nbSales", "text", "No sales");

          if (fetch.contract.created_at) {
            date = new Date(fetch.contract.created_at).toLocaleDateString("FR");
            displayData("#NFT__second__secondary__dateCreation", "text", date);
          } else {
            displayData("#NFT__second__secondary__dateCreation", "text", "No date of creation");
          }

          fetch.collection.name ? displayData("#NFT__second__principal__collection", "text", fetch.collection.name) : displayData("#NFT__second__principal__collection", "text", "No collection name");
          fetch.contract.address ? displayData("#NFT__second__secondary__address", "text", fetch.contract.address) : displayData("#NFT__second__secondary__address", "text", "No address");
          fetch.permalink ? displayData("#NFT__second__secondary__permalink", "text", "<a target=\"_blank\" href=\"".concat(fetch.permalink, "\">").concat(fetch.permalink, "</a>")) : displayData("#NFT__second__secondary__permalink", "text", "No permalink");
          fetch.contract.owner ? displayData("#NFT__second__secondary__owner", "text", fetch.contract.owner) : displayData("#NFT__second__secondary__owner", "text", "No owner");

        case 16:
        case "end":
          return _context.stop();
      }
    }
  });
};

exports.nft = nft;