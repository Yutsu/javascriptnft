//Single NFT

import { fetchData } from "../../Services/Api.js"
import { getAnyElement, renderCircleLoader } from "../../Utils/index.js"

export const nft = async () => {
  const paramsQuery = window.location.search
  const urlParams = new URLSearchParams(paramsQuery)
  const id = urlParams.get("id")

  const fetch = await fetchData(`nft/${id}`)

  const displayData = (el, type, data) => {
    if (type == "text" && getAnyElement(el)) {
      return (getAnyElement(el).innerHTML = data)
    } else if (type == "image" && getAnyElement(el)) {
      return getAnyElement(el).setAttribute("src", data)
    }
  }

  fetch.name
    ? displayData("#NFT__second__principal__name", "text", fetch.name)
    : displayData("#NFT__second__principal__name", "text", "No name")
  fetch.description
    ? displayData("#NFT__description", "text", fetch.description)
    : displayData("#NFT__description", "text", "No description")
  fetch.image_url ? displayData("#NFT__image", "image", fetch.image_url) : ""
  fetch.sales
    ? displayData("#NFT__second__secondary__nbSales", "text", fetch.sales)
    : displayData("#NFT__second__secondary__nbSales", "text", "No sales")
  if (fetch.contract.created_at) {
    let date = new Date(fetch.contract.created_at).toLocaleDateString("FR")
    displayData("#NFT__second__secondary__dateCreation", "text", date)
  } else {
    displayData(
      "#NFT__second__secondary__dateCreation",
      "text",
      "No date of creation"
    )
  }
  fetch.collection.name
    ? displayData(
        "#NFT__second__principal__collection",
        "text",
        fetch.collection.name
      )
    : displayData(
        "#NFT__second__principal__collection",
        "text",
        "No collection name"
      )
  fetch.contract.address
    ? displayData(
        "#NFT__second__secondary__address",
        "text",
        fetch.contract.address
      )
    : displayData("#NFT__second__secondary__address", "text", "No address")
  fetch.permalink
    ? displayData(
        "#NFT__second__secondary__permalink",
        "text",
        `<a target="_blank" href="${fetch.permalink}">${fetch.permalink}</a>`
      )
    : displayData("#NFT__second__secondary__permalink", "text", "No permalink")
  fetch.contract.owner
    ? displayData(
        "#NFT__second__secondary__owner",
        "text",
        fetch.contract.owner
      )
    : displayData("#NFT__second__secondary__owner", "text", "No owner")
}
