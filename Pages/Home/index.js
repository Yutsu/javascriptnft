import { fetchData, getNftPerPage } from "../../Services/Api.js"
import { nft } from "../nft/index.js"
import {
  getAnyElement,
  renderArticleCard,
  renderCircleLoader,
  renderCollectionArticle,
} from "../../Utils/index.js"

export const home = async () => {
  try {
    //fetch data
    const { assets } = await fetchData("")
    const {
      name,
      image_url,
      creator,
      sales,
      description,
      owner,
      contract,
      permalink,
    } = assets[14]
    const date = new Date(contract.created_at).toLocaleDateString("FR")

    const collectionsData = await fetchData("collections")
    const collections = collectionsData.collections
    const nftPageOne = await getNftPerPage(1)

    //get class
    const nft_name = getAnyElement(".nft__name")
    const nft_image = getAnyElement(".nft__image")
    const nft_username = getAnyElement(".nft__username")
    const nft_price = getAnyElement(".nft__price")
    const nft_description = getAnyElement(".nft__description")
    const nft_date = getAnyElement(".nft__date")
    const nft_owner = getAnyElement(".nft__owner")
    const nft_discover = getAnyElement(".nft__discover")
    const nft_collections_container = getAnyElement(".nft__collections")
    const nft_cards_pagination = getAnyElement(".container__all__Nft .cards")
    const nft_card_pagination = getAnyElement(".container__all__Nft .card")
    const prevBtn = getAnyElement(".prev")
    const nextBTn = getAnyElement(".next")
    const clickEvent = new MouseEvent("onclick")
    const pageNumbers = document.querySelectorAll(".pagination a")
    const cards = getAnyElement(".cards")
    const card = getAnyElement(".card")

    //render data
    nft_image && nft_image.setAttribute("src", image_url)
    nft_discover && nft_discover.setAttribute("href", permalink)
    nft_name ? (nft_name.innerText = name) : null
    nft_username ? (nft_username.innerText += creator.username) : null
    nft_price ? (nft_price.innerText += sales) : null
    nft_description ? (nft_description.innerText = description) : null
    nft_date ? (nft_date.innerText += date) : null
    nft_owner ? (nft_owner.innerText += owner.username) : null

    //cards nominees + renderCircleLoader
    const cards_nomenees_container = getAnyElement(".cards.nominees .card ")
    cards_nomenees_container.innerHTML = renderCircleLoader()
    const nomenees_loader = getAnyElement(".circle_loader")

    // setTimeout(() => {
    if (assets) {
      nomenees_loader.remove()
      for (let i = 0; i < 12; i++) {
        card ? (card.innerHTML += renderArticleCard(assets[i])) : null
      }
      if (cards) cards.appendChild(card)
    }
    // }, 2000);

    // collections
    nft_collections_container.innerHTML = renderCircleLoader()

    const collectionLoader = getAnyElement(".nft__collections .circle_loader")

    setTimeout(() => {
      collectionLoader.remove()
      collections.map((collection) => {
        if (nft_card_pagination) {
          return (nft_collections_container.innerHTML +=
            renderCollectionArticle(collection))
        }
      })
    }, 3500)

    // pagination
    const activePage = (e) => {
      const el = document.querySelector(".active")
      if (el) {
        el.classList.remove("active")
      }
      e.target.classList.add("active")
    }

    const getPrevPage = async () => {
      const activePage = getAnyElement(".active")
      const n = Number(activePage.textContent)
      if (activePage.textContent == "1") {
        activePage.previousElementSibling
        return
      }
      activePage.previousElementSibling.className = "active"
      activePage.previousSibling.nextSibling.classList.remove("active")
      let nftPerPage = await getNftPerPage(`${Number(n) - 1}`)
      let display = nftPerPage.assets.map((nft) => renderArticleCard(nft))
      if (nft_card_pagination) {
        nft_card_pagination.innerHTML = display.join("")
      }
      nft_cards_pagination &&
        nft_cards_pagination.appendChild(nft_card_pagination)
    }

    const getNextPage = async () => {
      const activePage = getAnyElement(".active")
      if (activePage.nextElementSibling == nextBTn) {
        return
      }
      const n = Number(activePage.textContent)
      activePage.nextSibling.nextSibling.className = "active"
      activePage.nextSibling.previousSibling.classList.remove("active")
      let nftPerPage = await getNftPerPage(`${Number(n) + 1}`)
      let display = nftPerPage.assets.map((nft) => renderArticleCard(nft))
      if (nft_card_pagination) {
        nft_card_pagination.innerHTML = display.join("")
      }
      nft_cards_pagination &&
        nft_cards_pagination.appendChild(nft_card_pagination)
    }

    const renderNft = (allNft) => {
      const { assets } = allNft
      let display = assets.map((nft) => renderArticleCard(nft))
      if (nft_card_pagination) {
        nft_card_pagination.innerHTML = display.join("")
      }
      nft_cards_pagination &&
        nft_cards_pagination.appendChild(nft_card_pagination)
    }

    //nft_cards_pagination.innerHTML = renderCircleLoader()

    renderNft(nftPageOne)

    const handlePagination = async (e) => {
      const n = e.path[0].textContent
      const nft = await getNftPerPage(n)
      renderNft(nft)
      return nft
    }

    const collectionsElements = document.querySelectorAll("article.collections")
    let favorites_icon = document.querySelectorAll(".favorite")

    collectionsElements.forEach((el) => {
      el.dispatchEvent(clickEvent)
      el.onclick = handleCollectionModal
    })

    pageNumbers.forEach((a) => {
      a.dispatchEvent(clickEvent)
      a.onclick = handlePagination
      a.onclick = activePage
    })

    favorites_icon.forEach((fav) => {
      fav.dispatchEvent(clickEvent)
      fav.onclick = handleFavorites
    })

    prevBtn.dispatchEvent(clickEvent)
    prevBtn.dispatchEvent(clickEvent)

    prevBtn.onclick = getPrevPage
    nextBTn.onclick = getNextPage

    // favorites
    const favorites = JSON.parse(localStorage.getItem("favorites")) || []

    function handleFavorites(e) {
      favorites_icon = document.querySelectorAll(".favorite")
      favorites_icon.forEach((fav) => {
        if (e.target == fav.childNodes[1]) {
          const id = fav.parentElement.id
          const index = favorites.indexOf(id)

          if (!~index) {
            fav.classList.add("is")
            favorites.push(id)
          }
          if (~index) {
            fav.classList.remove("is")
            favorites.splice(index, 1)
          }
          localStorage.setItem("favorites", JSON.stringify(favorites))
        }
      })
      console.log(localStorage)
    }

    const checkFavorites = () => {
      favorites_icon.forEach((fav) => {})
    }
    checkFavorites()

    const articlesElements = document.querySelectorAll("article.article")

    articlesElements.forEach((el) => {
      el.addEventListener("click", (e) => {
        if (e.target.tagName != "path" || e.target.tagName == "svg") {
          route(`/nft?id=${el.id}`)
          nft()
        }
      })
    })
  } catch (error) {
    console.log(error)
  }
}
