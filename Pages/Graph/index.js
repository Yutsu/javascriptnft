import { fetchData, getAllNft } from "../../Services/Api.js"
import { ascendingOrderOccur, renderSurnameLength } from "./Utils.js"

export const renderSalesGraphs = async () => {
  const nfts = await getAllNft()
  const nftSales = nfts.map((nft) => {
    return nft.sales
  })

  const data = {
    labels: ascendingOrderOccur(nftSales).nftUniq,
    datasets: [
      {
        label: "Number of sales",
        backgroundColor: "rgb(190, 155, 242)",
        borderColor: "rgb(190, 155, 242)",
        data: ascendingOrderOccur(nftSales).nftOccurence,
      },
    ],
  }

  const config = {
    type: "line",
    data: data,
    options: {
      plugins: {
        legend: {
          labels: {
            font: {
              size: 20,
            },
          },
        },
      },
      scales: {
        x: {
          title: {
            display: true,
            text: "Sales",
          },
        },
      },
    },
  }
  Chart.defaults.color = "white"
  const myChartSales = new Chart(document.getElementById("nftSales"), config)
}

export const renderStatsGraphs = async () => {
  const nfts = await getAllNft()
  const dataCreators = await fetchData("creators")
  const dataOwners = await fetchData("owners")

  const nftSymbol = nfts.map((nft) => {
    return nft.contract.symbol
  })

  const data = {
    labels: ["Creator", "Owner", "Contract symbol"],
    datasets: [
      {
        label: "Polar Area Chart",
        backgroundColor: [
          "rgb(255, 99, 132)",
          "rgb(75, 192, 192)",
          "rgb(255, 205, 86)",
        ],
        data: [
          renderSurnameLength(dataCreators.creators),
          renderSurnameLength(dataOwners.owners),
          ascendingOrderOccur(nftSymbol).nftOccurence.length,
        ],
      },
    ],
  }

  const config = {
    type: "polarArea",
    data: data,
    options: {
      plugins: {
        legend: {
          labels: {
            font: {
              size: 20,
            },
          },
        },
      },
    },
  }

  Chart.defaults.color = "white"
  const myChartStats = new Chart(document.getElementById("nftStats"), config)
  myChartStats.canvas.parentNode.style.height = "100%"
  myChartStats.canvas.parentNode.style.width = "50%"
}

export const renderPresaleGraphs = async () => {
  const nfts = await getAllNft()

  const nftPresales = nfts.map((nft) => {
    return nft.presale
  })

  let countPresaleTrue = 0
  let countPresaleFalse = 0
  nftPresales.map((presale) => {
    presale ? countPresaleTrue++ : countPresaleFalse++
  })

  const data = {
    labels: ["Presale true", "Presale false"],
    datasets: [
      {
        label: "Doughnut Chart",
        backgroundColor: ["rgb(250, 126, 97)", "rgb(194, 231, 218)"],
        data: [countPresaleTrue, countPresaleFalse],
      },
    ],
  }

  const config = {
    type: "doughnut",
    data: data,
    options: {
      plugins: {
        legend: {
          labels: {
            font: {
              size: 20,
            },
          },
        },
      },
    },
  }

  Chart.defaults.color = "white"
  const myChartPresales = new Chart(
    document.getElementById("nftPresales"),
    config
  )
  myChartPresales.canvas.parentNode.style.height = "100%"
  myChartPresales.canvas.parentNode.style.width = "50%"
}

export const renderContractSymbolGraphs = async () => {
  const nfts = await getAllNft()

  const nftSymbol = nfts.map((nft) => {
    return nft.contract.symbol
  })

  const data = {
    labels: ascendingOrderOccur(nftSymbol).nftUniq,
    datasets: [
      {
        label: "Contract symbol Chart",
        backgroundColor: [
          "rgba(255, 99, 132)",
          "rgba(255, 159, 64)",
          "rgba(255, 205, 86)",
          "rgba(75, 192, 192)",
          "rgba(54, 162, 235)",
        ],
        borderColor: [
          "rgb(255, 99, 132)",
          "rgb(255, 159, 64)",
          "rgb(255, 205, 86)",
          "rgb(75, 192, 192)",
          "rgb(54, 162, 235)",
        ],
        borderWidth: 1,
        data: ascendingOrderOccur(nftSymbol).nftOccurence,
      },
    ],
  }

  const config = {
    type: "bar",
    data: data,
    options: {
      plugins: {
        legend: {
          labels: {
            font: {
              size: 20,
            },
          },
        },
      },
    },
    scales: {
      y: {
        beginAtZero: true,
      },
    },
  }
  Chart.defaults.color = "white"
  const myChartContractSymbol = new Chart(
    document.getElementById("nftContractSymbol"),
    config
  )
}

export const renderAllGraph = () => {
  renderSalesGraphs()
  renderStatsGraphs()
  renderPresaleGraphs()
  renderContractSymbolGraphs()
}
