"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.renderAllGraph = exports.renderContractSymbolGraphs = exports.renderPresaleGraphs = exports.renderStatsGraphs = exports.renderSalesGraphs = void 0;

var _Api = require("../../Services/Api.js");

var _Utils = require("./Utils.js");

var renderSalesGraphs = function renderSalesGraphs() {
  var nfts, nftSales, data, config, myChartSales;
  return regeneratorRuntime.async(function renderSalesGraphs$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return regeneratorRuntime.awrap((0, _Api.getAllNft)());

        case 2:
          nfts = _context.sent;
          nftSales = nfts.map(function (nft) {
            return nft.sales;
          });
          data = {
            labels: (0, _Utils.ascendingOrderOccur)(nftSales).nftUniq,
            datasets: [{
              label: "Number of sales",
              backgroundColor: "rgb(190, 155, 242)",
              borderColor: "rgb(190, 155, 242)",
              data: (0, _Utils.ascendingOrderOccur)(nftSales).nftOccurence
            }]
          };
          config = {
            type: "line",
            data: data,
            options: {
              plugins: {
                legend: {
                  labels: {
                    font: {
                      size: 20
                    }
                  }
                }
              },
              scales: {
                x: {
                  title: {
                    display: true,
                    text: "Sales"
                  }
                }
              }
            }
          };
          Chart.defaults.color = "white";
          myChartSales = new Chart(document.getElementById("nftSales"), config);

        case 8:
        case "end":
          return _context.stop();
      }
    }
  });
};

exports.renderSalesGraphs = renderSalesGraphs;

var renderStatsGraphs = function renderStatsGraphs() {
  var nfts, dataCreators, dataOwners, nftSymbol, data, config, myChartStats;
  return regeneratorRuntime.async(function renderStatsGraphs$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return regeneratorRuntime.awrap((0, _Api.getAllNft)());

        case 2:
          nfts = _context2.sent;
          _context2.next = 5;
          return regeneratorRuntime.awrap((0, _Api.fetchData)("creators"));

        case 5:
          dataCreators = _context2.sent;
          _context2.next = 8;
          return regeneratorRuntime.awrap((0, _Api.fetchData)("owners"));

        case 8:
          dataOwners = _context2.sent;
          nftSymbol = nfts.map(function (nft) {
            return nft.contract.symbol;
          });
          data = {
            labels: ["Creator", "Owner", "Contract symbol"],
            datasets: [{
              label: "Polar Area Chart",
              backgroundColor: ["rgb(255, 99, 132)", "rgb(75, 192, 192)", "rgb(255, 205, 86)"],
              data: [(0, _Utils.renderSurnameLength)(dataCreators.creators), (0, _Utils.renderSurnameLength)(dataOwners.owners), (0, _Utils.ascendingOrderOccur)(nftSymbol).nftOccurence.length]
            }]
          };
          config = {
            type: "polarArea",
            data: data,
            options: {
              plugins: {
                legend: {
                  labels: {
                    font: {
                      size: 20
                    }
                  }
                }
              }
            }
          };
          Chart.defaults.color = "white";
          myChartStats = new Chart(document.getElementById("nftStats"), config);
          myChartStats.canvas.parentNode.style.height = "100%";
          myChartStats.canvas.parentNode.style.width = "50%";

        case 16:
        case "end":
          return _context2.stop();
      }
    }
  });
};

exports.renderStatsGraphs = renderStatsGraphs;

var renderPresaleGraphs = function renderPresaleGraphs() {
  var nfts, nftPresales, countPresaleTrue, countPresaleFalse, data, config, myChartPresales;
  return regeneratorRuntime.async(function renderPresaleGraphs$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.next = 2;
          return regeneratorRuntime.awrap((0, _Api.getAllNft)());

        case 2:
          nfts = _context3.sent;
          nftPresales = nfts.map(function (nft) {
            return nft.presale;
          });
          countPresaleTrue = 0;
          countPresaleFalse = 0;
          nftPresales.map(function (presale) {
            presale ? countPresaleTrue++ : countPresaleFalse++;
          });
          data = {
            labels: ["Presale true", "Presale false"],
            datasets: [{
              label: "Doughnut Chart",
              backgroundColor: ["rgb(250, 126, 97)", "rgb(194, 231, 218)"],
              data: [countPresaleTrue, countPresaleFalse]
            }]
          };
          config = {
            type: "doughnut",
            data: data,
            options: {
              plugins: {
                legend: {
                  labels: {
                    font: {
                      size: 20
                    }
                  }
                }
              }
            }
          };
          Chart.defaults.color = "white";
          myChartPresales = new Chart(document.getElementById("nftPresales"), config);
          myChartPresales.canvas.parentNode.style.height = "100%";
          myChartPresales.canvas.parentNode.style.width = "50%";

        case 13:
        case "end":
          return _context3.stop();
      }
    }
  });
};

exports.renderPresaleGraphs = renderPresaleGraphs;

var renderContractSymbolGraphs = function renderContractSymbolGraphs() {
  var nfts, nftSymbol, data, config, myChartContractSymbol;
  return regeneratorRuntime.async(function renderContractSymbolGraphs$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.next = 2;
          return regeneratorRuntime.awrap((0, _Api.getAllNft)());

        case 2:
          nfts = _context4.sent;
          nftSymbol = nfts.map(function (nft) {
            return nft.contract.symbol;
          });
          data = {
            labels: (0, _Utils.ascendingOrderOccur)(nftSymbol).nftUniq,
            datasets: [{
              label: "Contract symbol Chart",
              backgroundColor: ["rgba(255, 99, 132)", "rgba(255, 159, 64)", "rgba(255, 205, 86)", "rgba(75, 192, 192)", "rgba(54, 162, 235)"],
              borderColor: ["rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)", "rgb(75, 192, 192)", "rgb(54, 162, 235)"],
              borderWidth: 1,
              data: (0, _Utils.ascendingOrderOccur)(nftSymbol).nftOccurence
            }]
          };
          config = {
            type: "bar",
            data: data,
            options: {
              plugins: {
                legend: {
                  labels: {
                    font: {
                      size: 20
                    }
                  }
                }
              }
            },
            scales: {
              y: {
                beginAtZero: true
              }
            }
          };
          Chart.defaults.color = "white";
          myChartContractSymbol = new Chart(document.getElementById("nftContractSymbol"), config);

        case 8:
        case "end":
          return _context4.stop();
      }
    }
  });
};

exports.renderContractSymbolGraphs = renderContractSymbolGraphs;

var renderAllGraph = function renderAllGraph() {
  renderSalesGraphs();
  renderStatsGraphs();
  renderPresaleGraphs();
  renderContractSymbolGraphs();
};

exports.renderAllGraph = renderAllGraph;