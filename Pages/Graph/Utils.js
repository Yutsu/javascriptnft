export const getOccurrence = (array, value) => {
  let count = 0
  array.forEach((v) => v === value && count++)
  return count
}

export const ascendingOrderOccur = (nftSales) => {
  // order
  const salesAscendingOrder = nftSales.sort(function (a, b) {
    return a - b
  })

  // remove duplicate data
  const nftUniq = [...new Set(salesAscendingOrder)]

  // count occurence
  const nftOccurence = nftUniq.map((data) => {
    return getOccurrence(nftSales, data)
  })

  return {
    nftUniq: nftUniq,
    nftOccurence: nftOccurence,
  }
}

export const renderSurnameLength = (data) => {
  return [...new Set([...data.map((user) => user.username)])].length
}
