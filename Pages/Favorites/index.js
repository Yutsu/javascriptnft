import { getAllNft } from "../../Services/Api.js"
import { getAnyElement, renderNftFavCard } from "../../Utils/index.js"

export const favoritesPage = async () => {
  try {
    const favoriteStore = JSON.parse(localStorage.favorites)
    const nfts = await getAllNft()
    const container = getAnyElement(".container_favorites")
    const clickEvent = new MouseEvent("onclick")

    const newFavorites = nfts.filter(
      (nft) => `${nft.id}` === favoriteStore.find((id) => id === `${nft.id}`)
    )

    const renderFavorites = () => {
      let display = newFavorites.map((nft) => renderNftFavCard(nft))
      if (container) return (container.innerHTML = display.join(""))
    }

    renderFavorites()

    const favorites_icon = document.querySelectorAll(".favorite")

    // favorites
    const favorites = JSON.parse(localStorage.getItem("favorites")) || []

    favorites_icon.forEach((fav) => {
      fav.dispatchEvent(clickEvent)
      fav.onclick = handleFavorites
    })

    function handleFavorites(e) {
      favorites_icon.forEach((fav) => {
        if (e.target == fav.childNodes[1]) {
          const id = fav.parentElement.id
          const index = favorites.indexOf(id)

          if (~index) {
            fav.classList.remove("is")
            favorites.splice(index, 1)
            setTimeout(() => {
              fav.parentElement.remove()
            }, 1500)
          }
          localStorage.setItem("favorites", JSON.stringify(favorites))
        }
      })
    }

    const checkFavorites = () => {
      favorites_icon.forEach((fav) => {})
    }

    checkFavorites()
  } catch (error) {
    console.log(error)
  }
}
