import { getAllNft } from "../../../Services/Api.js"
import {
  getAnyElement,
  getId,
  renderArticleCard,
} from "../../../Utils/index.js"

export const sales = async () => {
  try {
    //Fetch Data
    const dataAll = await getAllNft()

    //Input
    const cards_filter_sales = getAnyElement(".cards")
    const card_filter_sales = getAnyElement(".card")
    const priceInput = getId("priceInput")
    const priceValue = getAnyElement(".priceValue")
    priceValue.textContent = `Sales: 9 999`

    //Render NFT card
    const renderEth = (filterPrice) => {
      let display = filterPrice.map((data) => {
        return renderArticleCard(data)
      })
      card_filter_sales.innerHTML = display.join("")
      cards_filter_sales.appendChild(card_filter_sales)
    }

    //Filter price
    const priceFilter = () => {
      const value = parseInt(priceInput.value)
      priceValue.textContent = `Sales: ${value.toLocaleString()}`
      const filterPrice = dataAll.filter((nft) => {
        return nft.sales <= value
      })
      renderEth(filterPrice)
    }

    //Event
    window.addEventListener("DOMContentLoaded", priceFilter())
    priceInput.addEventListener("input", priceFilter)
  } catch (error) {
    console.log(error)
  }
}
