"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sales = void 0;

var _Api = require("../../../Services/Api.js");

var _index = require("../../../Utils/index.js");

var sales = function sales() {
  var dataAll, cards_filter_sales, card_filter_sales, priceInput, priceValue, renderEth, priceFilter;
  return regeneratorRuntime.async(function sales$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return regeneratorRuntime.awrap((0, _Api.getAllNft)());

        case 3:
          dataAll = _context.sent;
          //Input
          cards_filter_sales = (0, _index.getAnyElement)(".cards");
          card_filter_sales = (0, _index.getAnyElement)(".card");
          priceInput = (0, _index.getId)("priceInput");
          priceValue = (0, _index.getAnyElement)(".priceValue");
          priceValue.textContent = "Sales: 9 999"; //Render NFT card

          renderEth = function renderEth(filterPrice) {
            var display = filterPrice.map(function (data) {
              return (0, _index.renderArticleCard)(data);
            });
            card_filter_sales.innerHTML = display.join("");
            cards_filter_sales.appendChild(card_filter_sales);
          }; //Filter price


          priceFilter = function priceFilter() {
            var value = parseInt(priceInput.value);
            priceValue.textContent = "Sales: ".concat(value.toLocaleString());
            var filterPrice = dataAll.filter(function (nft) {
              return nft.sales <= value;
            });
            renderEth(filterPrice);
          }; //Event


          window.addEventListener("DOMContentLoaded", priceFilter());
          priceInput.addEventListener("input", priceFilter);
          _context.next = 18;
          break;

        case 15:
          _context.prev = 15;
          _context.t0 = _context["catch"](0);
          console.log(_context.t0);

        case 18:
        case "end":
          return _context.stop();
      }
    }
  }, null, null, [[0, 15]]);
};

exports.sales = sales;