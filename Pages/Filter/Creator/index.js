import { fetchData, getAllNft } from "../../../Services/Api.js"
import {
  getAnyElement,
  renderArticleCard,
  renderCreatorCard,
  addStyle,
  createElement,
} from "../../../Utils/index.js"

export const creators = async () => {
  try {
    // fetching data
    const data = await fetchData("creators")
    let creatorsName = [
      ...new Set([...data.creators.map((creator) => creator.username)]),
    ]
    const allNFT = await getAllNft()

    // creating and getting elements
    const cards_filter_creators = getAnyElement(".cards")
    const card_filter_creators = getAnyElement(".card")
    const card_creator_profile = getAnyElement(".creator")
    const container = getAnyElement(".container.creators label")
    const select = createElement("select", container)
    const clickEvent = new MouseEvent("onchange")
    const placeholder =
      "<option value='' disabled selected hidden>Select a creator to see his work...</option>"

    select.setAttribute("name", "creators")
    select.setAttribute("id", "creators")
    select.setAttribute("aria-busy", false)
    select.innerHTML += placeholder
    addStyle(select, "width", "40%")

    // creating options
    creatorsName &&
      creatorsName.map((creator) => {
        let option = new Option(`${creator}`, `${creator}`)
        return select.appendChild(option)
      })

    // render NFT cards
    const renderNft = (allNft) => {
      let display = allNft.map((nft) => renderArticleCard(nft))
      card_filter_creators.innerHTML = display.join("")
      cards_filter_creators.appendChild(card_filter_creators)
    }
    renderNft(allNFT)

    const filterByCreator = (creator) => {
      let filteredByCreator = allNFT.filter((ntf) => {
        return ntf.creator.username === creator
      })
      return filteredByCreator
    }

    const filterCreator = (creator) => {
      let searchedCreator = data.creators.filter((c) => c.username === creator)
      return searchedCreator
    }

    const renderFilteredNft = (filteredByCreator) => {
      let display = filteredByCreator.map((nft) => renderArticleCard(nft))
      card_filter_creators.innerHTML = display.join("")
      cards_filter_creators.appendChild(card_filter_creators)
    }

    const handleCreatorSelection = (e) => {
      e.preventDefault()
      card_creator_profile.innerHTML = renderCreatorCard(
        filterCreator(e.target.value)
      )
      renderFilteredNft(filterByCreator(e.target.value))
    }

    select.dispatchEvent(clickEvent)
    select.onchange = handleCreatorSelection
  } catch (error) {
    console.log(error)
  }
}
